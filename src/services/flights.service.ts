import { BadRequestError, NotFoundError } from 'routing-controllers'
import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from "../models/persons.model"

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async onBoard(flightId:string, personId:string) {
        const flight = await FlightsModel.findById(flightId)

        if (!flight) {
            throw new NotFoundError("Flight not found")
        }
        const person = await PersonsModel.findById(personId)
        if (!person) {
            throw new NotFoundError("Person not found")
        }

        if(flight.passengers.includes(personId)){
            throw new BadRequestError("User already on board")
        }
        else {
            flight.passengers.push(personId)
        }
        flight.save()
        return;
    }
}
