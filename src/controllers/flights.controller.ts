import { JsonController, Get, Post, Param, HttpCode } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @HttpCode(204)
    @Post('/:flightId/onBoard/:personId/', { transformResponse: false })
    async onBoard(@Param('flightId') flightId: string, @Param('personId') personId: string) {
        await flightsService.onBoard(flightId, personId)
        return {
            status: 200
        }
    }
}
